//Obtenemos los elementos del documento

var boton = document.getElementById("boton");

var resultado = document.getElementById("output");

var ingreso_datos = document.getElementById("input");


//Agregamos un evento al boton

boton.addEventListener("click", doOption);


//Función global de las opciones

function doOption()
{
    var option = parseInt(document.getElementById("select-options").value);

    ingreso_datos.innerHTML = "";
    resultado.innerHTML = "";

    switch(option)
    {
        case 1:
            var fecha = new Date();

            var fecha_segundos = (fecha.getHours()*60*60) + (fecha.getMinutes()*60) + fecha.getSeconds();

            resultado.innerHTML = "<b> Hora actual: </b>" + fecha.getHours() + ":" + ((fecha.getMinutes()<10?"0":"") + fecha.getMinutes()) + ":" + ((fecha.getSeconds()<10?"0":"") + fecha.getSeconds()) + "<br/>" 
                                + "<b>Hora actual en segundos: </b>" + fecha_segundos;
        break;

        case 2:
            ingreso_datos.innerHTML = '<br/> Base: '
                                    + '<input type="text" id="base">'
                                    + '<br/> Altura: '
                                    + '<input type="text" id="altura">'
                                    + '<input type="button" value="Calcular" onclick="calcularAreaTriangulo()">';
        break;

        case 3:
            ingreso_datos.innerHTML = '<br/>Ingrese su número: '
                                    + '<input type="text" id="raiz">'
                                    + '<input type="button" value="Calcular" onclick="raizNumeroImpar()">';
        break;

        case 4:
            ingreso_datos.innerHTML = '<br/>Ingrese su texto: '
                                    + '<input type="text" id="texto">'
                                    + '<input type="button" value="Calcular" onclick="calcularCadena()">';
        break;

        case 5:
            var array1 = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes"];
            var array2 = ["Sábado", "Domingo"];
            var array3 = array1.concat(array2);
            resultado.innerHTML = "<b>Array 1: </b>" + array1
                                + "<br/>"
                                + "<b>Array 2: </b>" + array2
                                + "<br/>"
                                + "<b>Array concat: </b>" + array3;
        break;

        case 6:
            resultado.innerHTML = navigator.userAgent;
        break;

        case 7:
            resultado.innerHTML = "<b>Ancho: </b>" + screen.width + "px" + "<br/><b>Alto: </b>" + screen.height + "px";
        break;

        case 8:
            window.print();
        break;

        default:
            alert("No ha elegido ninguna opción.");
        break;
    }
    
}

//Funciones

function calcularCadena()
{

    var texto = document.getElementById("texto");
    resultado.innerHTML = "<b> Longitud de la cadena: </b>" + texto.value.length;

}

function calcularAreaTriangulo()
{

    var base = parseInt(document.getElementById("base").value);
    var altura = parseInt(document.getElementById("altura").value);
    var area = (base * altura) / 2;
    if(isNaN(area))
    {
        alert ("Ambos datos deben ser números");
    }
    else
    {
        resultado.innerHTML = "<b> Area del triángulo: </b>" + area;
    }
}

function raizNumeroImpar()
{
    var raiz = parseInt(document.getElementById("raiz").value);
    var raiz_resultado;
    if(isNaN(raiz + 1))
    {
        alert ("Debe ingresar un número");
    }
    else if( (parseInt(raiz) % 2) != 0 )
    {
        raiz_resultado = Math.sqrt(raiz);
        resultado.innerHTML = "<b>Raíz: </b>" + raiz_resultado.toFixed(3);
    }
    else
    {
        alert("Por favor ingrese un número impar");
    }
}
